/***************************************************
 ** @Desc : This file for 商户列表
 ** @Time : 2019.04.02 9:19
 ** @Author : Joker
 ** @File : merchant
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.02 9:19
 ** @Software: GoLand
 ****************************************************/

let xf_merchant = {
    add_xf_merchant: function () {
        let merchant_no = $("#merchant_no").val();
        let key = $("#key").val();
        if (merchant_no === "" || key === "") {
            toastr.error("商户编号或密钥不能为空!");
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/add_xf/",
                data: {
                    xf_merchant_no: merchant_no,
                    xf_key: key,
                    xf_merchant_name: $("#merchant_name").val()
                },
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        $("#add_account_modal").modal('hide');
                        toastr.success("添加成功！");
                        setInterval(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    edit_merchant_ui: function (edit_id) {
        $.ajax({
            type: "GET",
            url: "/merchant/edit_xf_ui/" + edit_id,
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $("#edit_label_name").html(res.data.MerchantName);
                    $("#edit_merchant_name").val(res.data.MerchantName);
                    $("#edit_merchant_no").val(res.data.MerchantNo);
                    $("#edit_key").val(res.data.SecretKey);

                    $('#edit_account_modal').modal({backdrop: 'static', keyboard: false});
                } else {
                    toastr.error("此商户不存在！");
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    edit_xf_merchant: function () {
        let merchant_no = $("#edit_merchant_no").val();
        let key = $("#edit_key").val();
        if (merchant_no === "" || key === "") {
            toastr.error("商户编号或密钥不能为空!");
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/edit_xf/",
                data: {
                    xf_merchant_no: merchant_no,
                    xf_key: key,
                    xf_merchant_name: $("#edit_merchant_name").val()
                },
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        $("#edit_account_modal").modal('hide');
                        toastr.success("更新成功！");
                        setInterval(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    del_account: function (del) {
        swal({
            title: "Are you sure?",
            text: "删除此商户无法恢复！",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "get",
                    url: "/merchant/del_xf/" + del,
                    success: function (res) {
                        if (res.code === 9) {
                            swal(res.msg, {
                                icon: "success",
                                closeOnClickOutside: false,
                            }).then(() => {
                                setInterval(function () {
                                    window.location.reload();
                                }, 2000);
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                                closeOnClickOutside: false,
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                    }
                });
            }
        });
    },
    send_msg_edit_userInfo: function () {
        let cemail = $("#cemail").val();
        let new_cemail = $("#new_cemail").val();
        let patrn = /^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){5,19}$/;
        if (cemail === "" || new_cemail === "") {
            toastr.error("密码不能为空!");
        } else if (!patrn.exec(new_cemail)) {
            toastr.error("密码只能输入6-20个以字母开头、可带数字、“_”、“.”的字串!");
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/edit_userInfo_sms/",
                data: $("#commentForm").serialize(),
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        toastr.success(res.msg);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    edit_userInfo: function () {
        let cemail = $("#cemail").val();
        let new_cemail = $("#new_cemail").val();
        let mobile_code = $("#mobile_code").val();
        let patrn = /^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){5,19}$/;
        if (cemail === "" || new_cemail === "" || mobile_code === "") {
            toastr.error("密码或者手机验证码不能为空!");
        } else if (!patrn.exec(new_cemail)) {
            toastr.error("密码只能输入6-20个以字母开头、可带数字、“_”、“.”的字串!");
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/edit_userInfo/",
                data: $("#commentForm").serialize(),
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        toastr.success("更新成功！");
                        setInterval(function () {
                            window.location.href = "/IsRight";
                        }, 2000)
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    query_balance: function (no) {
        $.ajax({
            type: "GET",
            url: "/merchant/query_xf_balance/" + no,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    swal("更新成功！", {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        setInterval(function () {
                            window.location.reload();
                        }, 2000);
                    });
                } else {
                    swal(res.msg, {
                        icon: "warning",
                        closeOnClickOutside: false,
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    }
};