/***************************************************
 ** @Desc : This file for 计算记录金额
 ** @Time : 2019.04.15 14:08 
 ** @Author : Joker
 ** @File : record_factor
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.15 14:08
 ** @Software: GoLand
****************************************************/
package condition

import (
	"recharge/models"
)

type RecordFactor struct{}

var rechargeMdl = models.RechargeRecord{}
var payMdl = models.WithdrawRecord{}
var transferMdl = models.TransferRecord{}

// 计算成功充值金额
func (*RecordFactor) CountSuccessRecharge(in map[string]interface{}) (rechargeT float64, count int) {
	recordBy := rechargeMdl.SelectAllRechargeRecordBy(in)
	for _, v := range recordBy {
		rechargeT += v.ReAmount
	}
	return rechargeT, len(recordBy)
}

// 计算成功充值金额，加上费率和手续费
func (*RecordFactor) CountSuccessRechargeHaveFee(in map[string]interface{}, info models.UserInfo) (rechargeT, fee float64, l int) {
	recordBy := rechargeMdl.SelectAllRechargeRecordBy(in)
	for _, v := range recordBy {
		f := v.ReAmount*info.RechargeRate*0.001 + info.RechargeFee
		fee += f
		rechargeT += v.ReAmount - f
	}
	l = len(recordBy)
	return
}

// 计算成功提现金额
func (*RecordFactor) CountSuccessPay(in map[string]interface{}) (rechargeT float64, count int) {
	withdrawRecordBy := payMdl.SelectAllWithdrawRecordBy(in)
	for _, v := range withdrawRecordBy {
		rechargeT += v.WhAmount
	}
	return rechargeT, len(withdrawRecordBy)
}

// 计算成功提现金额，加上费率和手续费
func (*RecordFactor) CountSuccessPayHaveFee(in map[string]interface{}, userInfo models.UserInfo) (rechargeT, fee float64, l int) {
	withdrawRecordBy := payMdl.SelectAllWithdrawRecordBy(in)
	for _, v := range withdrawRecordBy {
		fee += userInfo.PayFee
		rechargeT += v.WhAmount + userInfo.PayFee
	}
	l = len(withdrawRecordBy)
	return
}

// 计算成功B2C金额
func (*RecordFactor) CountSuccessTransfer(in map[string]interface{}) (rechargeT float64) {
	transferRecords := transferMdl.SelectAllTransferRecordBy(in)
	for _, v := range transferRecords {
		rechargeT += v.TrAmount
	}
	return rechargeT
}

// 计算成功B2C金额，加上费率和手续费
func (*RecordFactor) CountSuccessTransferHaveFee(in map[string]interface{}, userInfo models.UserInfo) (rechargeT, fee float64, l int) {
	transferRecords := transferMdl.SelectAllTransferRecordBy(in)
	for _, v := range transferRecords {
		f := v.TrAmount * userInfo.RechargeRate * 0.001
		fee += v.TrAmount * userInfo.RechargeRate * 0.001
		rechargeT += v.TrAmount - f
	}
	l = len(transferRecords)
	return
}
