/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019/6/6 11:13
 ** @Author : Joker
 ** @File : recharge_v2_test
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019/6/6 11:13
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"encoding/json"
	"fmt"
	"recharge/controllers/implement"
	"recharge/models"
	"recharge/utils"
	"testing"
)

var AES = utils.AES{}
var merchantImpl = implement.MerchantImpl{}

func TestDecodeResp(t *testing.T) {
	body := `{"sign":"PWylHVdgqCIbW9WOj9JmLqkGFivtHahoz3rc54UTr3+794qvdhKeBuSd/vTIBHFEFnEw68Y1J6Dwpqv6Brysvmhwl2le36buIzHkuI18b7FwASP6waQtqvfotNLtG0kjT7N11j2UWpfIsC9i9s3LNJMtrcZD6Pxn8rk8ghHhYq/NBuNnwkrEmtJav4bZBXfvZ8iZ9zsbr0tGkcZfRa9oe4hVLU6Y4PyBvvnXuQioXeiFEfwH+DoJcuHKi8h+dN1HmF8xgcGkMyfwej5Pu1m+g/3QsbfBM87Y+0XHh/nV+keBv01AJf9jNSEVpleQywE/YSxisAqUDziyUV0reRNDmw==","message":"SUCCESS","tm":"h65N4Qnu3+7rLFzxA/mzqcqM9/RWJw6YeFQItLAuWtCYhGAeWECsTN6OGI7ejVDHnaF3LpxUKb7VJUjAeHPSSwAEIhnu1fo6SeCA9YuSegfv30LBqlsHz8vTBoCsqRqyZ/cEe/cATyxdU/dlL6gY+nxxLCIlfJtFwrnLfWCOzN8mkUvunFXG/KIrRJrekiGDiMQS1LKuWly8QNl6b07wlMQ/hLsI4z1yWQlkpK91iSgSJyQCpOLQnmcjpRHd/wjVXBEnGNyXVJIQEndkNTtN3dssqM0jzqU6ix0U+Ve18rd1NulfMXAF9BhnoJyV2MTUghcmB+QEN/3NYisO4OxwCw==","data":"yfz5PqeCOuYUG5dGrHxhZxcBZGGQSrYH7gLsAESWF5k9JV+yCb4dnLQIaVXeAZoQJdoWj52nFyHe6vu6VPtD11bfgyf055MwD16FUyHb/0C2Sde3zxfnsI6TedeNvqYpH2JREyBZbw+TiX/q3K4o15uJQpv5TskNrgy+XS8ssyvhLiwkhHPkbCgSNxv05gmDAWms4GsJHVULu0MMB8JVtv4KlWh5upQY/16yrgtyydI=","merchantId":"M200012155","code":"99000"}`
	respData := models.RespData{}
	err := json.Unmarshal([]byte(body), &respData)
	fmt.Println("错误1:", err)
	fmt.Println("数据1：", respData)

	//b := "6815b9f5fc4091976cc13f5870d05801"
	s, err := merchantImpl.DecryptRespDataByPrivateKey(respData.Tm)
	fmt.Println("错误2:", err)
	fmt.Println("数据2：", string(s))

	bytes, err := AES.AesDecryptV2([]byte(respData.Data), s)
	fmt.Println("错误3:", err)
	fmt.Println("数据3：", string(bytes))

}
