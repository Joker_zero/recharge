/***************************************************
 ** @Desc : This file for 首页跳转
 ** @Time : 2019/6/13 15:07
 ** @Author : Joker
 ** @File : home
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019/6/13 15:07
 ** @Software: GoLand
****************************************************/
package controllers

import (
	"github.com/astaxie/beego"
)

type HomeAct struct {
	beego.Controller
}

// 加载首页面
// @router / [get,post]
func (the *HomeAct) ShowHome() {
	the.TplName = "home.html"
}

// 开发文档页面
// @router /document [get,post]
func (the *HomeAct) ShowDocument() {
	the.TplName = "document.html"
}
