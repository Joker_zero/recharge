/***************************************************
 ** @Desc : This file for 转账实现方法
 ** @Time : 2019.04.23 16:59 
 ** @Author : Joker
 ** @File : transfer_impl
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.23 16:59
 ** @Software: GoLand
****************************************************/
package implement

import (
	"recharge/utils"
	"regexp"
	"strconv"
)

type TransferImpl struct{}

// 验证b2c转账信息
func (*TransferImpl) VerificationTransferInfo(money string) (string, bool) {
	matched1, _ := regexp.MatchString(`^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$`, money)
	if !matched1 {
		return "请输入正确的充值金额哦!", false
	}

	float, _ := strconv.ParseFloat(money, 10)
	if float > utils.SingleMaxForTransfer {
		return "转账金额超限!", false
	}
	return "", true
}
