/***************************************************
 ** @Desc : This file for 对接数据格式
 ** @Time : 2019.04.19 16:11 
 ** @Author : Joker
 ** @File : api_data
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.19 16:11
 ** @Software: GoLand
****************************************************/
package models

// 对接充值请求参数
type ApiRechargeRequestParams struct {
	MerchantNo   string //商户编号
	OrderPrice   string //订单金额
	OutOrderNo   string //商户充值订单号
	TradeTime    string //交易下单时间
	AccountNo    string //银行卡号
	AccountName  string //银行账号对应的户名
	RecevieBank  string //备付金账户标识
	ApiNotifyUrl string //后台异步通知地址
	Item         string //扩展字段
	Sign         string //签名串
}

// 对接查询请求参数
type ApiQueryParams struct {
	MerchantNo string //商户编号
	OutTradeNo string //商户充值订单号
	Sign       string //签名串
}

// 对接代付余额查询请求参数
type ApiQueryBalanceParams struct {
	MerchantNo string //商户编号
	Timestamp  string //时间戳
	Sign       string //签名串
}

// 对接代付请求参数
type ApiPayRequestParams struct {
	MerchantNo   string //商户编号
	OrderPrice   string //订单金额
	OutOrderNo   string //商户充值订单号
	TradeTime    string //交易下单时间
	UserType     string //用户类型
	AccountNo    string //银行卡号
	AccountName  string //银行账号对应的户名
	BankNo       string //银行编码
	Issuer       string //联行号
	ApiNotifyUrl string //后台异步通知地址
	Item         string //扩展字段
	Sign         string //签名串
}

// 对接异步通知参数
type ApiResponseBody struct {
	ResultCode   string `json:"resultCode"`
	Msg          string `json:"msg"`
	MerchantNo   string `json:"merchant_no"`    //商户号
	OutTradeNo   string `json:"out_trade_no"`   //交易订单号
	Amount       string `json:"amount"`         //金额
	AmountNotFee string `json:"amount_not_fee"` //扣除手续费金额
	AmountFee    string `json:"amount_fee"`     //手续费
	TradeStatus  string `json:"trade_status"`   //订单状态
	TradeTime    string `json:"trade_time"`     //交易完成时间
	TradeNo      string `json:"trade_no"`       //交易流水号
	Item         string `json:"item"`           //扩展字段
	Sign         string `json:"sign"`           //订单签名数据
}

// 对接查询响应
type ApiQueryResponseBody struct {
	OutTradeNo   string `json:"out_trade_no"`   //交易订单号
	OrderStatus  string `json:"order_status"`   //交易状态
	Amount       string `json:"amount"`         //订单金额
	AmountNotFee string `json:"amount_not_fee"` //扣除手续费金额
	AmountFee    string `json:"amount_fee"`     //手续费
	TradeNo      string `json:"trade_no"`       //交易流水号
	CompleteTime string `json:"complete_time"`  //完成时间
	Sign         string `json:"sign"`           //订单签名数据
}

// 对接代付余额查询响应
type ApiQueryBalanceResponseBody struct {
	MerchantNo string `json:"merchant_no"` //商户号
	TAmount    string `json:"t_amount"`    //总金额
	UAmount    string `json:"u_amount"`    //可用余额
	FAmount    string `json:"f_amount"`    //冻结金额,出款在途金额
	Sign       string `json:"sign"`        //签名数据
}
