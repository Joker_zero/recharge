/***************************************************
 ** @Desc : This file for 自定义加减款数据模型
 ** @Time : 2019.05.22 14:59 
 ** @Author : Joker
 ** @File : custom_plus_minus
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.05.22 14:59
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
	"recharge/sys"
	"recharge/utils"
)

type CustomPlusMinus struct {
	Id         int
	CreateTime string
	EditTime   string
	Version    int
	UserId     int
	UserName   string
	Addition   float64
	Deduction  float64
	HandingFee float64
}

func (*CustomPlusMinus) TableEngine() string {
	return "INNODB"
}

func (*CustomPlusMinus) TableName() string {
	return CustomPlusMinusTBName()
}

/* *
 * @Description: 添加加减款明细
 * @Author: Joker
 * @Date: 2019-5-22 15:05:44
 * @Param: Merchant
 * @return: int: 判断成功的标识
 * @return: int64:	在表中哪一行插入
 **/
func (*CustomPlusMinus) InsertCustomPlusMinus(w CustomPlusMinus) (int, int64) {
	om := orm.NewOrm()

	in, _ := om.QueryTable(CustomPlusMinusTBName()).PrepareInsert()
	id, err := in.Insert(&w)
	if err != nil {
		sys.LogError("InsertCustomPlusMinus failed to insert for: ", err)
		return utils.FAILED_FLAG, id
	}

	return utils.SUCCESS_FLAG, id
}

// 查询分页
func (*CustomPlusMinus) SelectCustomPlusMinusListPage(params map[string]interface{}, limit, offset int) (list []CustomPlusMinus) {
	om := orm.NewOrm()
	qt := om.QueryTable(CustomPlusMinusTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	_, err := qt.OrderBy("-create_time").Limit(limit, offset).All(&list)
	if err != nil {
		sys.LogError("SelectCustomPlusMinusListPage failed to query paging for: ", err)
	}
	return list
}

func (*CustomPlusMinus) SelectCustomPlusMinusCount(params map[string]interface{}) int {
	om := orm.NewOrm()
	qt := om.QueryTable(CustomPlusMinusTBName())
	for k, v := range params {
		if v != "" {
			qt = qt.Filter(k, v)
		}
	}
	cnt, err := qt.Count()
	if err != nil {
		sys.LogError("SelectCustomPlusMinusCount failed to count for:", err)
	}
	return int(cnt)
}

// 读取多个加减款明细记录通过 用户ID
func (*CustomPlusMinus) SelectOneCustomPlusMinusByUserId(id int) (cs []CustomPlusMinus) {
	om := orm.NewOrm()
	_, err := om.QueryTable(CustomPlusMinusTBName()).Filter("user_id", id).All(&cs)
	if err != nil {
		sys.LogError("SelectOneCustomPlusMinusByUserId failed to select one:", err)
	}
	return cs
}

