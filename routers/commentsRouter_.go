package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["recharge/controllers:ApiPay"] = append(beego.GlobalControllerRouter["recharge/controllers:ApiPay"],
		beego.ControllerComments{
			Method:           "ApiRecharge",
			Router:           "/api/pay/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:ApiPay"] = append(beego.GlobalControllerRouter["recharge/controllers:ApiPay"],
		beego.ControllerComments{
			Method:           "ApiRechargePay",
			Router:           "/api/query_pay/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:ApiPay"] = append(beego.GlobalControllerRouter["recharge/controllers:ApiPay"],
		beego.ControllerComments{
			Method:           "ApiRechargePayBalance",
			Router:           "/api/query_pay_balance/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:ApiRecharge"] = append(beego.GlobalControllerRouter["recharge/controllers:ApiRecharge"],
		beego.ControllerComments{
			Method:           "ApiRechargeQuery",
			Router:           "/api/query_recharge/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:ApiRecharge"] = append(beego.GlobalControllerRouter["recharge/controllers:ApiRecharge"],
		beego.ControllerComments{
			Method:           "ApiRecharge",
			Router:           "/api/recharge/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:AsyNotice"] = append(beego.GlobalControllerRouter["recharge/controllers:AsyNotice"],
		beego.ControllerComments{
			Method:           "B2CTransferNotice",
			Router:           "/notice/b2c_transfer_notice/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:AsyNotice"] = append(beego.GlobalControllerRouter["recharge/controllers:AsyNotice"],
		beego.ControllerComments{
			Method:           "XFPayNotice",
			Router:           "/notice/xf_pay_notice/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:AsyNotice"] = append(beego.GlobalControllerRouter["recharge/controllers:AsyNotice"],
		beego.ControllerComments{
			Method:           "XFRechargeNotice",
			Router:           "/notice/xf_recharge_notice/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:AsyNoticeV2"] = append(beego.GlobalControllerRouter["recharge/controllers:AsyNoticeV2"],
		beego.ControllerComments{
			Method:           "B2CTransferNotice",
			Router:           "/notice_v2/b2c_transfer_notice/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:AsyNoticeV2"] = append(beego.GlobalControllerRouter["recharge/controllers:AsyNoticeV2"],
		beego.ControllerComments{
			Method:           "XFPayNotice",
			Router:           "/notice_v2/xf_pay_notice/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:AsyNoticeV2"] = append(beego.GlobalControllerRouter["recharge/controllers:AsyNoticeV2"],
		beego.ControllerComments{
			Method:           "XFRechargeNotice",
			Router:           "/notice_v2/xf_recharge_notice/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:BulkPay"] = append(beego.GlobalControllerRouter["recharge/controllers:BulkPay"],
		beego.ControllerComments{
			Method:           "DoBulkPay",
			Router:           "/merchant/do_bulk_pay/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:BulkPay"] = append(beego.GlobalControllerRouter["recharge/controllers:BulkPay"],
		beego.ControllerComments{
			Method:           "SendMsgDoBulkPay",
			Router:           "/merchant/do_bulk_pay_sms/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:BulkPay"] = append(beego.GlobalControllerRouter["recharge/controllers:BulkPay"],
		beego.ControllerComments{
			Method:           "ShowBulkPayUI",
			Router:           "/merchant/show_bulk_pay_ui/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:BulkRecharge"] = append(beego.GlobalControllerRouter["recharge/controllers:BulkRecharge"],
		beego.ControllerComments{
			Method:           "DoBulkRecharge",
			Router:           "/merchant/do_bulk_recharge/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:BulkRecharge"] = append(beego.GlobalControllerRouter["recharge/controllers:BulkRecharge"],
		beego.ControllerComments{
			Method:           "ShowBulkRechargeUI",
			Router:           "/merchant/show_bulk_recharge_ui/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:HomeAct"] = append(beego.GlobalControllerRouter["recharge/controllers:HomeAct"],
		beego.ControllerComments{
			Method:           "ShowHome",
			Router:           "/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:HomeAct"] = append(beego.GlobalControllerRouter["recharge/controllers:HomeAct"],
		beego.ControllerComments{
			Method:           "ShowDocument",
			Router:           "/document",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Pay"] = append(beego.GlobalControllerRouter["recharge/controllers:Pay"],
		beego.ControllerComments{
			Method:           "DoRecharge",
			Router:           "/merchant/do_pay/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Pay"] = append(beego.GlobalControllerRouter["recharge/controllers:Pay"],
		beego.ControllerComments{
			Method:           "SendMsgDoPay",
			Router:           "/merchant/do_pay_sms/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Pay"] = append(beego.GlobalControllerRouter["recharge/controllers:Pay"],
		beego.ControllerComments{
			Method:           "PayNotice",
			Router:           "/merchant/pay_notice/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Pay"] = append(beego.GlobalControllerRouter["recharge/controllers:Pay"],
		beego.ControllerComments{
			Method:           "ShowPay",
			Router:           "/merchant/show_pay/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Pay"] = append(beego.GlobalControllerRouter["recharge/controllers:Pay"],
		beego.ControllerComments{
			Method:           "HandPayRecord",
			Router:           "/user/hand_pay_record/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Pay"] = append(beego.GlobalControllerRouter["recharge/controllers:Pay"],
		beego.ControllerComments{
			Method:           "ShowHandPayRecordUI",
			Router:           "/user/hand_pay_record_ui/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:PayQuery"] = append(beego.GlobalControllerRouter["recharge/controllers:PayQuery"],
		beego.ControllerComments{
			Method:           "PayQuery",
			Router:           "/merchant/pay_query/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:PayQueryV2"] = append(beego.GlobalControllerRouter["recharge/controllers:PayQueryV2"],
		beego.ControllerComments{
			Method:           "PayQuery",
			Router:           "/merchant/pay_query_v2/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:PayV2"] = append(beego.GlobalControllerRouter["recharge/controllers:PayV2"],
		beego.ControllerComments{
			Method:           "DoRecharge",
			Router:           "/merchant/do_pay_v2/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Recharge"] = append(beego.GlobalControllerRouter["recharge/controllers:Recharge"],
		beego.ControllerComments{
			Method:           "DoRecharge",
			Router:           "/merchant/do_recharge/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Recharge"] = append(beego.GlobalControllerRouter["recharge/controllers:Recharge"],
		beego.ControllerComments{
			Method:           "QueryAllMerchantByType",
			Router:           "/merchant/merchant_type/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Recharge"] = append(beego.GlobalControllerRouter["recharge/controllers:Recharge"],
		beego.ControllerComments{
			Method:           "RechargeNotice",
			Router:           "/merchant/recharge_notice/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Recharge"] = append(beego.GlobalControllerRouter["recharge/controllers:Recharge"],
		beego.ControllerComments{
			Method:           "ShowRecharge",
			Router:           "/merchant/show_recharge/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:RechargeQuery"] = append(beego.GlobalControllerRouter["recharge/controllers:RechargeQuery"],
		beego.ControllerComments{
			Method:           "RechargeQuery",
			Router:           "/merchant/recharge_query/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:RechargeQueryV2"] = append(beego.GlobalControllerRouter["recharge/controllers:RechargeQueryV2"],
		beego.ControllerComments{
			Method:           "RechargeQuery",
			Router:           "/merchant/recharge_query_v2/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:RechargeV2"] = append(beego.GlobalControllerRouter["recharge/controllers:RechargeV2"],
		beego.ControllerComments{
			Method:           "DoRechargeV2",
			Router:           "/merchant/do_recharge_v2/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "ShowUserTodayAmount",
			Router:           "/merchant/count_all_recharge_pay/?:params",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "DownloadRecordExcel",
			Router:           "/merchant/download_excel/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "PayQueryAndListPage",
			Router:           "/merchant/list_pay/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "ListPayUI",
			Router:           "/merchant/list_pay_ui/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "ListRechargeUI",
			Router:           "/merchant/list_recharge/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "QueryAndListPage",
			Router:           "/merchant/list_recharge_record/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "MakeExcelForRecord",
			Router:           "/merchant/make_excel/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "TransferQueryAndListPage",
			Router:           "/transfer/list_transfer/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Record"] = append(beego.GlobalControllerRouter["recharge/controllers:Record"],
		beego.ControllerComments{
			Method:           "ListTransferUI",
			Router:           "/transfer/show_transfer_record/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Transfer"] = append(beego.GlobalControllerRouter["recharge/controllers:Transfer"],
		beego.ControllerComments{
			Method:           "DoTransfer",
			Router:           "/transfer/do_transfer/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Transfer"] = append(beego.GlobalControllerRouter["recharge/controllers:Transfer"],
		beego.ControllerComments{
			Method:           "ShowPay",
			Router:           "/transfer/show_transfer/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:Transfer"] = append(beego.GlobalControllerRouter["recharge/controllers:Transfer"],
		beego.ControllerComments{
			Method:           "TransferQuery",
			Router:           "/transfer/transfer_query/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UCustomPlusMinus"] = append(beego.GlobalControllerRouter["recharge/controllers:UCustomPlusMinus"],
		beego.ControllerComments{
			Method:           "ListPlusMinusUI",
			Router:           "/merchant/list_pMinus/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UCustomPlusMinus"] = append(beego.GlobalControllerRouter["recharge/controllers:UCustomPlusMinus"],
		beego.ControllerComments{
			Method:           "QueryAndListPage",
			Router:           "/merchant/list_plus_minus/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "IsRightUser",
			Router:           "/IsRight",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "FlushCaptcha",
			Router:           "/flushCaptcha.py/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "ShowLoginUI",
			Router:           "/login",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "UserLogin",
			Router:           "/login.py/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "LoginOut",
			Router:           "/loginOut.py",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "SmsPhoneCode",
			Router:           "/sms.do/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserLogin"] = append(beego.GlobalControllerRouter["recharge/controllers:UserLogin"],
		beego.ControllerComments{
			Method:           "VerifyCaptcha",
			Router:           "/verifyCaptcha.py/:value/:chaId",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ActiveUser",
			Router:           "/user/active/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "AddUser",
			Router:           "/user/add_user/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "AdditionUser",
			Router:           "/user/addition_user/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ShowAdditionUserUI",
			Router:           "/user/addition_user_ui/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "DeductionUser",
			Router:           "/user/deduction_user/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ShowDeductionUserUI",
			Router:           "/user/deduction_user_ui/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "EditUser",
			Router:           "/user/edit_user/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ShowEditUserUI",
			Router:           "/user/edit_user_ui/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "FreezeUser",
			Router:           "/user/freeze/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "QueryAndListPage",
			Router:           "/user/list/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ShowLookUserUI",
			Router:           "/user/look_user_ui/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ResetUser",
			Router:           "/user/reset/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserManage"] = append(beego.GlobalControllerRouter["recharge/controllers:UserManage"],
		beego.ControllerComments{
			Method:           "ShowUserManageUI",
			Router:           "/user/user_manage_ui/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserReconcile"] = append(beego.GlobalControllerRouter["recharge/controllers:UserReconcile"],
		beego.ControllerComments{
			Method:           "QueryAndListPage",
			Router:           "/user/reconcile_list/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:UserReconcile"] = append(beego.GlobalControllerRouter["recharge/controllers:UserReconcile"],
		beego.ControllerComments{
			Method:           "ShowUserReconcileUI",
			Router:           "/user/reconcile_ui/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "ShowIndex",
			Router:           "/merchant/",
			AllowHTTPMethods: []string{"get", "post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "AddXFMerchant",
			Router:           "/merchant/add_xf/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "RemoveXFMerchant",
			Router:           "/merchant/del_xf/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "EditUserInfo",
			Router:           "/merchant/edit_userInfo/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "SendMsgEditUserInfo",
			Router:           "/merchant/edit_userInfo_sms/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "ShowEditUserInfoUI",
			Router:           "/merchant/edit_userInfo_ui/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "EditXFMerchant",
			Router:           "/merchant/edit_xf/?:params",
			AllowHTTPMethods: []string{"post"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "ShowEditXFUI",
			Router:           "/merchant/edit_xf_ui/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "ListMerchant",
			Router:           "/merchant/list_UI/",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "QueryAndListPage",
			Router:           "/merchant/list_xf/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

	beego.GlobalControllerRouter["recharge/controllers:XFMerchant"] = append(beego.GlobalControllerRouter["recharge/controllers:XFMerchant"],
		beego.ControllerComments{
			Method:           "XFQueryBalance",
			Router:           "/merchant/query_xf_balance/?:params",
			AllowHTTPMethods: []string{"get"},
			MethodParams:     param.Make(),
			Filters:          nil,
			Params:           nil})

}
