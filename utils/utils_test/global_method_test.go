/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.04.08 11:44 
 ** @Author : Joker
 ** @File : encryption_test.go
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.08 11:44
 ** @Software: GoLand
****************************************************/
package utils

import (
	"crypto/x509"
	"fmt"
	"net/url"
	"recharge/utils"
	"recharge/utils/interface_config"
	"strings"
	"testing"
)

var encrypt = utils.Encrypt{}
var key = `MIIBIjANBgkqhkiG9w0BAQEF MIIBCgKCAQEAsLEC28DJvFAlKFVxZNmV2xA40jBWFaePtSgFLOYHY7PVSziuImR+TmaI+7zDNIQGeR7f12JGlguH0H67lKorMZwDhixY0y6p/shGzbHb+Ifo8rX2Q8xkRDqEPUw5RpgKc1p1CaBgmfnsVgkWu9poiIlTSnTifwKZPX6Zg614GWbfAE0DbNGivC4CAFF8+1QwouYrRtgxqG6K4N1FPc1Asv+LfGlThudn48QDJGFWp/+IJh/jtis/RLBMQfMxEt4DUSgmt3Ph94Tr00KkQhFZ3Y+0OSYmDxHJ1xh6aipffjT0JhDW8/guQ3QyOP3bVSipTOFhEOI1oVLDAGzr744lbQIDAQAB`
var data = "exleRfpHgPQRfbiCB2cIGE0LA054oVLv5+vXP0gT6GyrR+ErllZnwVp2g92DCrtwjMgd4a+kNs7UGDyS824Hxg=="

func TestEncrypto(t *testing.T) {
	reqParams := url.Values{}
	reqParams.Add("servic", "REQ_QUERY_BALANCE")
	reqParams.Add("secId", interface_config.SECID)
	reqParams.Add("version", interface_config.VERSION)
	start := "-----BEGIN PUBLIC KEY-----\n"
	end := "\n-----END PUBLIC KEY-----"

	paramsMd5 := encrypt.EncodeMd5([]byte(reqParams.Encode()))
	s := start + key + end
	fmt.Println("key：", s)
	encrypts, err := encrypt.RsaEncrypto([]byte(strings.ToLower(paramsMd5)), []byte(s))
	fmt.Println("结果：", string(encrypts))
	fmt.Println("错误：", err)

	base64Encode := encrypt.Base64Encode(encrypts)
	fmt.Println("64结果：", base64Encode)
}

var AES = utils.AES{}

func TestECB2(t *testing.T) {
	decrypted, _ := AES.AesDecrypt([]byte(data), []byte(key))
	fmt.Println("解码：", string(decrypted))
}

func TestRsa(t *testing.T) {
	bytes, _ := encrypt.Base64Decode(key)
	derPkix, err := x509.MarshalPKIXPublicKey(bytes)
	fmt.Println(derPkix)
	fmt.Println(err)
	fmt.Println(string(bytes))
}